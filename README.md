# README #

QR Code Generator is a basic Python 3 script used to create QR code images via the command line.

## Requirements ##
* Python 3
	* [https://www.python.org/](https://www.python.org/)

* Python Modules
	* qrcode
		* [https://github.com/lincolnloop/python-qrcode/](https://github.com/lincolnloop/python-qrcode/)
	* Pillow
		* [https://python-pillow.org/](https://python-pillow.org/)
	* pathvalidate
		* [https://github.com/thombashi/pathvalidate/](https://github.com/thombashi/pathvalidate/)

## Version History ##
### v1.5.3 (2022 January 20) ###
* Added option to export QR code with rounded corners

### v1.5.2 (2022 January 19) ###
* Added ability to let user choose save destination
	* [QRCODE-4](https://brandonhines.atlassian.net/browse/QRCODE-4)

### v1.5.1 (2022 January 18) ###
* Added error message to let user know if an invalid color was entered
	* [QRCODE-1](https://brandonhines.atlassian.net/browse/QRCODE-1)

### v1.5 (2022 January 18) ###
* Added sanitization of user entered filenames to ensure broad platform compatibility

### v1.0 - Initial release (2022 January 18) ###
* Create a QR for text or web addresses
* Customize foreground and background color of QR code
* QR code is resolution 570 x 570 and about 4 kb in size
